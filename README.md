# Vagrant Samples #

Just to stash my portfolio use cases of Vagrant.

### What is this repository for? ###

* Easy and simple example that beginners can use.


### How do I get set up? ###

* Required softwares:
* [Vagrant download](https://www.vagrantup.com/downloads)
* [VBox download](https://www.virtualbox.org/wiki/Downloads)


### Who do I talk to? ###

* [Chow Kar Meng's homepage](http://chow.karmeng.my)
* [Vagrant HomePage](https://www.vagrantup.com/)
* [Vagrant Community Documentation](https://friendsofvagrant.github.io/v1/docs/index.html)
* [Virtualbox Page](https://www.virtualbox.org/)