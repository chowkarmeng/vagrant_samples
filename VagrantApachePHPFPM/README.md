# This vagrant project are intend to show case orchestration of vbox using vagrant #

The example will create a Apache, PHP-FPM and MySQL stack.

### What is this sub repo for? ###

* Show case vagrant to orchestrate vbox with turn-key readiness for LAMP in a docker vbox VM guest.

### Pre-requisites ###
* Required software:
* [Vagrant download](https://www.vagrantup.com/downloads)
* [VBox download](https://www.virtualbox.org/wiki/Downloads)

### How do I get set up? ###

* vbox version VirtualBox-6.1.18-142142-Win.exe installed
* vagrant vagrant_2.2.15_x86_64.msi installed
* once above done, do the following into command prompt (administrator privilleged command prompt not needed)
* cd localdev
* once vagrant up is done, to get into vm shell type the following
* vagrant ssh
* to access to main page, http://127.0.0.1
* refer to check the configuration of the LAMP server, visit http://127.0.0.1/info.php


### Who do I talk to? ###

* [Vagrant HomePage](https://www.vagrantup.com/)
* [Vagrant Community Documentation](https://friendsofvagrant.github.io/v1/docs/index.html)
* [Virtualbox Page](https://www.virtualbox.org/)