# ElasticSearch limitation #

Make sure in the vm host add:
* /etc/sysctl.conf has added vm.max_map_count=262144
* ElasticSearch 7.17.3 are used instead of 8.2.0 for ease of demo

### What is this repository for? ###

* Easy and simple example that beginners can configure ElasticSearch.
