yum install -y vim telnet dos2unix zip unzip

# use docker-ce
yum install -y yum-utils device-mapper-persistent-data lvm2 
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y install docker-ce docker-ce-cli containerd.io docker-compose-plugin

# make docker engine as daemon systemctl
systemctl enable docker
systemctl start docker

# Disable firewall
systemctl disable firewalld
systemctl stop firewalld

# Disable selinux for docker
setenforce 0
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

# Restart and refresh docker daemon 
systemctl restart docker